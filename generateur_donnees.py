# -*- coding: utf-8 -*-
"""
Created on Tue Sep 17 11:44:30 2019

@author: caged2013
"""
from random import choice, randint

from faker import Faker


fake = Faker('fr_FR')


def chèque_émis():
    somme = randint(1000, 50000)/100
    numero = randint(100, 1000)
    return {"type": "chèque émis",
            "numéro": numero,
            'montant': -somme,
            "date de valeur": 
                fake.date_between(start_date='-30d', 
                    end_date='today').strftime("%d/%m/%Y")
            }

def retrait():
    somme = randint(1000, 50000)/100
    return {"type": "retrait",
            'montant': -somme,
            "date de valeur": 
                fake.date_between(start_date='-30d', 
                    end_date='today').strftime("%d/%m/%Y")
            }   

def dépôt():
    somme = randint(1000, 50000)/100
    return {"type": "dépôt",
            'montant': somme,
            "date de valeur": 
                fake.date_between(start_date='-30d', 
                    end_date='today').strftime("%d/%m/%Y")
            }
        
def virement_reçu():
    somme = randint(1000, 50000)/100
    return {"type": "virement reçu",
            'montant': somme,
            "date de valeur": 
                fake.date_between(start_date='-30d', 
                    end_date='today').strftime("%d/%m/%Y")
                }
                
def virement_émis():
    somme = randint(1000, 50000)/100
    return {"type": "virement émis",
            'montant': -somme,
            "date de valeur": 
                fake.date_between(start_date='-30d', 
                    end_date='today').strftime("%d/%m/%Y")
                }
    
def operation_bancaire():
    opération = choice([retrait, 
           chèque_émis,
           dépôt, 
           virement_reçu, 
           virement_émis])
    return opération()

def operations_bancaires(n):
    opérations = [operation_bancaire() for k in range(n)]
    opérations.sort(key=lambda x: x["date de valeur"], reverse=True)
    return opérations

def donnees_bancaires(nombre_clients, nombres_opérations=5):
    donnees = []
    for compteur in range(nombre_clients):
        donnees.append({
                "num": fake.pyint(),
                "nom": fake.last_name(),
                "prénom": fake.first_name(),
                "adresse": fake.address(),
                "historique": operations_bancaires(nombres_opérations)
                })
    return donnees


def ecrit_valeur(valeur, fichier, decalage=0):
    fichier.write(decalage*4*" ")
    if isinstance(valeur, list):
        ecrit_liste(valeur, fichier, decalage=decalage+1)
    elif isinstance(valeur, float):
        fichier.write(str(valeur))
    elif isinstance(valeur, int):
        fichier.write(str(valeur))
    elif isinstance(valeur, str) and '\n' in valeur:
        fichier.write('"""' + valeur + '"""')
    elif isinstance(valeur, str):
        fichier.write('"' + valeur + '"')



def ecrit_dictionnaire(dictionnaire, fichier, decalage=0):
    fichier.write(decalage*4*" ")
    fichier.write("{\n")
    for cle in dictionnaire :
        ecrit_valeur(cle, fichier, decalage=decalage + 1)
        fichier.write(': ')
        ecrit_valeur(dictionnaire[cle], fichier)
        fichier.write(",\n")
    fichier.write(decalage*4*" ")
    fichier.write("}")

def ecrit_liste(liste, fichier, decalage=0):
    fichier.write(decalage*4*" ")
    fichier.write("[\n")
    for numero, dictionnaire in enumerate(liste) :
        ecrit_dictionnaire(dictionnaire, fichier, decalage=decalage + 1)
        if not numero == len(liste) - 1 :
            fichier.write(',')
        fichier.write('\n')
            
    fichier.write(decalage*4*" ")
    fichier.write("]\n")
        
if __name__ == "__main__":
    print(operations_bancaires(10))
    données_theme1 = donnees_bancaires(10)
    with open("données-TP3.py", 'w', encoding="utf-8") as fichier:
        fichier.write("données_bancaires = ")
        ecrit_liste(données_theme1, fichier)
        
