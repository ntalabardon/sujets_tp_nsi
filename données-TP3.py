
données_bancaires = [
    {
        "num": 1720,
        "nom": "Blanchard",
        "prénom": "Céline",
        "adresse": """46, avenue Philippine Begue
59012 Lecoq-sur-Goncalves""",
        "historique": [
            {
                "type": "chèque émis",
                "numéro": 474,
                "montant": -328.24,
                "date de valeur": "24/08/2019",
            },
            {"type": "dépôt", "montant": 325.7, "date de valeur": "24/08/2019"},
            {
                "type": "virement émis",
                "montant": -347.23,
                "date de valeur": "15/09/2019",
            },
            {"type": "dépôt", "montant": 452.52, "date de valeur": "13/09/2019"},
            {"type": "dépôt", "montant": 373.62, "date de valeur": "09/09/2019"},
        ],
    },
    {
        "num": 8007,
        "nom": "Allard",
        "prénom": "Françoise",
        "adresse": """37, rue Suzanne Vidal
78745 Robin""",
        "historique": [
            {"type": "retrait", "montant": -390.35, "date de valeur": "30/08/2019"},
            {"type": "retrait", "montant": -151.36, "date de valeur": "11/09/2019"},
            {"type": "dépôt", "montant": 364.78, "date de valeur": "04/09/2019"},
            {
                "type": "virement émis",
                "montant": -332.62,
                "date de valeur": "03/09/2019",
            },
            {"type": "retrait", "montant": -293.19, "date de valeur": "02/09/2019"},
        ],
    },
    {
        "num": 2342,
        "nom": "Remy",
        "prénom": "Nicolas",
        "adresse": """31, chemin de Blanchet
20303 Lemoine""",
        "historique": [
            {
                "type": "chèque émis",
                "numéro": 866,
                "montant": -146.15,
                "date de valeur": "31/08/2019",
            },
            {"type": "retrait", "montant": -329.19, "date de valeur": "29/08/2019"},
            {"type": "retrait", "montant": -486.77, "date de valeur": "22/08/2019"},
            {
                "type": "chèque émis",
                "numéro": 717,
                "montant": -497.13,
                "date de valeur": "10/09/2019",
            },
            {"type": "retrait", "montant": -205.95, "date de valeur": "07/09/2019"},
        ],
    },
    {
        "num": 8236,
        "nom": "Lefebvre",
        "prénom": "Olivier",
        "adresse": """2, avenue Delaunay
18010 Barbe""",
        "historique": [
            {
                "type": "chèque émis",
                "numéro": 864,
                "montant": -31.84,
                "date de valeur": "12/09/2019",
            },
            {
                "type": "chèque émis",
                "numéro": 638,
                "montant": -351.1,
                "date de valeur": "06/09/2019",
            },
            {"type": "dépôt", "montant": 388.94, "date de valeur": "05/09/2019"},
            {"type": "dépôt", "montant": 130.49, "date de valeur": "02/09/2019"},
            {
                "type": "chèque émis",
                "numéro": 747,
                "montant": -486.3,
                "date de valeur": "01/09/2019",
            },
        ],
    },
    {
        "num": 6122,
        "nom": "Richard",
        "prénom": "Antoinette",
        "adresse": """53, rue de Gaudin
93758 Richard""",
        "historique": [
            {"type": "dépôt", "montant": 148.34, "date de valeur": "27/08/2019"},
            {
                "type": "virement émis",
                "montant": -430.41,
                "date de valeur": "24/08/2019",
            },
            {"type": "retrait", "montant": -308.89, "date de valeur": "15/09/2019"},
            {
                "type": "virement reçu",
                "montant": 115.13,
                "date de valeur": "04/09/2019",
            },
            {"type": "retrait", "montant": -253.26, "date de valeur": "01/09/2019"},
        ],
    },
    {
        "num": 4137,
        "nom": "Raynaud",
        "prénom": "Capucine",
        "adresse": """8, chemin de Daniel
42677 Sainte Pierre""",
        "historique": [
            {"type": "virement reçu", "montant": 167.1, "date de valeur": "28/08/2019"},
            {
                "type": "virement reçu",
                "montant": 138.16,
                "date de valeur": "20/08/2019",
            },
            {"type": "retrait", "montant": -358.95, "date de valeur": "16/09/2019"},
            {
                "type": "chèque émis",
                "numéro": 285,
                "montant": -254.89,
                "date de valeur": "15/09/2019",
            },
            {
                "type": "chèque émis",
                "numéro": 137,
                "montant": -211.49,
                "date de valeur": "05/09/2019",
            },
        ],
    },
    {
        "num": 8395,
        "nom": "Thibault",
        "prénom": "Gilles",
        "adresse": """avenue Leduc
20815 Sainte Étienne""",
        "historique": [
            {
                "type": "virement émis",
                "montant": -380.16,
                "date de valeur": "26/08/2019",
            },
            {"type": "retrait", "montant": -259.5, "date de valeur": "26/08/2019"},
            {
                "type": "chèque émis",
                "numéro": 693,
                "montant": -375.11,
                "date de valeur": "23/08/2019",
            },
            {
                "type": "virement émis",
                "montant": -266.58,
                "date de valeur": "13/09/2019",
            },
            {"type": "virement reçu", "montant": 97.87, "date de valeur": "07/09/2019"},
        ],
    },
    {
        "num": 3646,
        "nom": "Allain",
        "prénom": "daisy",
        "adresse": """139, rue Christiane Begue
62850 Gaillard-sur-Ollivier""",
        "historique": [
            {
                "type": "chèque émis",
                "numéro": 250,
                "montant": -13.21,
                "date de valeur": "30/08/2019",
            },
            {"type": "retrait", "montant": -496.83, "date de valeur": "25/08/2019"},
            {
                "type": "chèque émis",
                "numéro": 475,
                "montant": -482.84,
                "date de valeur": "20/08/2019",
            },
            {
                "type": "chèque émis",
                "numéro": 567,
                "montant": -190.69,
                "date de valeur": "05/09/2019",
            },
            {
                "type": "chèque émis",
                "numéro": 298,
                "montant": -162.46,
                "date de valeur": "04/09/2019",
            },
        ],
    },
    {
        "num": 5790,
        "nom": "Gregoire",
        "prénom": "Émilie",
        "adresse": """20, rue de Dos Santos
15929 Lamynec""",
        "historique": [
            {"type": "dépôt", "montant": 481.34, "date de valeur": "26/08/2019"},
            {
                "type": "chèque émis",
                "numéro": 721,
                "montant": -397.43,
                "date de valeur": "16/09/2019",
            },
            {
                "type": "virement reçu",
                "montant": 343.03,
                "date de valeur": "16/09/2019",
            },
            {
                "type": "chèque émis",
                "numéro": 487,
                "montant": -165.57,
                "date de valeur": "16/09/2019",
            },
            {
                "type": "chèque émis",
                "numéro": 425,
                "montant": -346.26,
                "date de valeur": "02/09/2019",
            },
        ],
    },
    {
        "num": 7285,
        "nom": "Millet",
        "prénom": "Arthur",
        "adresse": """6, rue Baudry
40915 Barthelemy""",
        "historique": [
            {
                "type": "virement reçu",
                "montant": 407.04,
                "date de valeur": "30/08/2019",
            },
            {"type": "retrait", "montant": -460.14, "date de valeur": "19/08/2019"},
            {"type": "retrait", "montant": -75.47, "date de valeur": "18/08/2019"},
            {"type": "dépôt", "montant": 253.29, "date de valeur": "06/09/2019"},
            {
                "type": "chèque émis",
                "numéro": 787,
                "montant": -61.96,
                "date de valeur": "03/09/2019",
            },
        ],
    },
]
